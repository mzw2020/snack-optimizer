# Snack Optimizer

#### 介绍
    这个仓库中存储的是蛇优化算法讲解的PPT和对应的python代码
    如果你需要听论文和代码的讲解请到下面的地址：https://www.bilibili.com/video/BV1ZT411J7pQ?


#### 参考文献
    文献名称：Snake Optimizer: A novel meta-heuristic optimization algorithm
    
    matlab代码下载的地址为：https://ww2.mathworks.cn/matlabcentral/fileexchange/106465-snake-optimizer

#### 本地环境

1.  numpy
2.  matplotlib
3.  python3.8
